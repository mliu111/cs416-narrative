// SCREEN INFO
const WIDTH = window.screen.width;
const HEIGHT = window.screen.height;

// MAP INFO (1300x scale) https://github.com/topojson/us-atlas
const M_WIDTH = 975;
const M_HEIGHT = 610;
const M_SCALE = 1300;

// DATASET INFO
const D_MAX_STATE_COUNT = 1228;
const D_TOTAL_CASES = 4626;
const D_TOTAL_STATES = 48;
const D_AVG_CASES = 4626 / 48;
const D_MIN_DATE = Date.parse('2022/5/18'); // first reported case 
const D_MAX_DATE = Date.parse('2022/7/27'); // last reported case 

// date utils (https://stackoverflow.com/questions/3224834/get-difference-between-2-dates-in-javascript)
const DATE_MS_DAY = 1000 * 60 * 60 * 24;
function DAY_DIFF(a, b) {
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

    return Math.floor((utc2 - utc1) / DATE_MS_DAY);
};

// color utils - we manually separate some colors
const COLOR_DOMAIN = [-1, -0.5, -0.2, 0, 0.2, 0.5, 1];
const COLOR_RANGE = ['#4a7d7c', '#73a3a5', '#aecbce', '#aecbce', '#d7b7b7', '#c37e76', '#c37e76'];
const color_scale = d3.scaleLinear().domain(COLOR_DOMAIN).range(COLOR_RANGE); // blue to orange

// projection utils
const projection = d3.geoAlbersUsa().scale(M_SCALE).translate([487.5, 305]);
const path = d3.geoPath().projection(projection);


// storage
let selection = 0;
let states = null; // state topojson
let mkp = null; // mkp data (cumulative to each date)
let mkplm = new Map(); // line maps (e.g. {state_id: [{'date': date, 'case': 1}]} )

function drawLine(cidx) {
    document.getElementById('interactive').style.display = 'none';
    document.getElementById('interactive1').style.display = '';
    // get data
    let data = mkplm.get(cidx);
    // helper functions
    const x = d3.scaleTime()
        .domain([D_MIN_DATE, D_MAX_DATE])
        .range([0, 900]);
    const y = d3.scaleLinear()
        .domain([0, d3.max(data, d => d.case)])
        .range([500, 0]);
    const line = d3.line().x(d => x(Date.parse(d.date))).y(d => y(d.case));
    const reveal = (path) => {
        const length = path.node().getTotalLength();
        path.transition()
            .duration(5000)
            .ease(d3.easeLinear)
            .attrTween("stroke-dasharray", () => {
                return d3.interpolate(`0,${length}`, `${length},${length}`);
            });
    };
    // clear old line
    document.getElementById('line').innerHTML = '';
    let svg = d3.select('#line')
        .append('svg')
        .attr("width", M_WIDTH)
        .attr("height", M_HEIGHT);
    // draw x
    svg.append("g")
      .attr("transform", "translate(0," + 500 + ")")
      .call(d3.axisBottom(x));
    // draw y
    svg.append("g")
      .call(d3.axisLeft(y).ticks(900 / 40));
    // draw new line
    svg.selectAll("path")
        .data(data)
        .enter()
        .append('path')
        .attr('fill', 'none')
        .attr('d', line(data))
        .attr("stroke", "steelblue")
        .call(reveal)
        .append('title')
        .text((d) => `${d.date}: ${d.case}`);
}

function drawMap(idx) {
    document.getElementById('interactive1').style.display = 'none';
    document.getElementById('interactive').style.display = '';
    let date = dateConverter(idx);
    // clear old map
    document.getElementById('map').innerHTML = '';
    // create new map
    d3.select("#map")
        .append("svg")
        .attr('class', 'map')
        .attr("width", M_WIDTH)
        .attr("height", M_HEIGHT)
        .append('g')
        .selectAll("path")
        .data(states.features)
        .enter()
        .append("path")
        .attr("d", path)
        .attr("stroke", "grey")
        .attr('data-count', (d) => mkp.get(d.id)[date])
        .attr('data-id', (d) => d.id)
        .style("fill", (d) => {
            return color_scale(mkp.get(d.id)[date] / D_AVG_CASES)
        })
        .on("click", (ev) => drawLine(ev.currentTarget.attributes['data-id'].value))
        .append('title')
        .text((d) => `${mkp.get(d.id).State}: ${mkp.get(d.id)[date]}\nClick for change over time`);
    // create legend
    let legend = Legend(d3.scaleLinear(COLOR_DOMAIN, COLOR_RANGE), {
        title: "Reported Cases vs Average (%)",
    });
    document.getElementById('map').appendChild(legend);
};

// load json from server. This might take some time.
// everything else happens after data loads
Promise.all([
    fetch('./mpx-cum-count.json'),
    fetch('./states-10m.json'),
]).then(responses => Promise.all(responses.map(response => response.json())))
    .then((data) => {
        const MKP_DATA = data[0];
        const US_DATA = data[1];

        states = topojson.feature(US_DATA, US_DATA.objects.states);
        const state_name_map = new Map(states.features.map(d => [d.properties.name, d.id]));

        // parse the monkeypox data
        mkp = new Map(states.features.map(d => [d.id, {}]));
        MKP_DATA.forEach((state) => {
            mkp.set(state_name_map.get(state.State), state)

            let tmp = [];
            DATE.forEach((date) => {
                tmp.push({ 'date': date, 'case': state[date] });
            });
            mkplm.set(state_name_map.get(state.State), tmp);
        });

        document.getElementById('mySlider').addEventListener("input", function () {
            selection = document.getElementById('mySlider').value;
            document.getElementById('slider-time').textContent = `Date: ${dateConverter(selection)}`;
            drawMap(selection);
        }, false);

        document.getElementById('map-button').addEventListener("click", function () {
            drawMap(selection);
        }, false);

        // initialize a map with first day
        selection = 0;
        document.getElementById('slider-time').textContent = `Date: ${dateConverter(selection)}`
        drawMap(selection);

    }).catch(function (error) {
        console.log(error);
    });